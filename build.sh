#!/bin/sh
set -o errexit
set -o nounset

export RUSTFLAGS="-Dwarnings"

echo "========== FORMAT CODE =========="
status_before=$(git status --porcelain)
cargo fmt
if [ "$status_before" != "$(git status --porcelain)" ]; then
    echo "There were changes in the formatting. Aborting build."
    exit 1
fi
echo "============ CLIPPY ============="
cargo clippy
echo "============= TEST =============="
cargo test
