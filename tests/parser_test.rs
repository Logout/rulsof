extern crate rulsof;

use rulsof::parser;
use std::collections::HashMap;
mod test_output;

const FIRST_PID: &'static str = "9097";

#[test]
fn reads_all_fields_in_a_record() {
    let single_line_result =
        parser::parse_lsof_output(test_output::EXAMPLE_SINGLE_LINE_OUTPUT).unwrap();

    let single_line_map = single_line_result.get(FIRST_PID).unwrap();
    let process_fields = single_line_map.process_fields.clone();

    let element_count = 6;
    assert_eq!(element_count, process_fields.len());

    let expected_output = [
        ("p", "9097"),
        ("g", "9097"),
        ("R", "23617"),
        ("c", "zsh"),
        ("u", "1000"),
        ("L", "logout"),
    ]
    .iter()
    .cloned()
    .map(|(l, r)| (String::from(l), String::from(r)))
    .collect::<HashMap<String, String>>();
    assert_eq!(expected_output, process_fields);
}

#[test]
fn reads_all_records_for_a_process() {
    let single_process_result =
        parser::parse_lsof_output(test_output::EXAMPLE_SINGLE_PROCESS_OUTPUT).unwrap();

    let single_process_map = single_process_result.get(FIRST_PID).unwrap();
    let file_fields = &single_process_map.files;
    let element_count = 3;
    assert_eq!(element_count, file_fields.len());

    let expected_process_fields = [
        ("p", "9097"),
        ("g", "9097"),
        ("R", "23617"),
        ("c", "zsh"),
        ("u", "1000"),
        ("L", "logout"),
    ]
    .iter()
    .cloned()
    .map(|(l, r)| (String::from(l), String::from(r)))
    .collect::<HashMap<String, String>>();
    assert_eq!(expected_process_fields, single_process_map.process_fields);

    let expected_file_records = test_output::EXAMPLE_SINGLE_PROCESS_OUTPUT_FILE_RECORDS
        .iter()
        .cloned()
        .map(|a| {
            a.iter()
                .cloned()
                .map(|(l, r)| (String::from(l), String::from(r)))
                .collect::<HashMap<String, String>>()
        })
        .collect::<Vec<HashMap<String, String>>>();
    assert_eq!(expected_file_records, single_process_map.files);
}

#[test]
fn creates_process_map_from_lsof_output() {
    let lsof_result = parser::parse_lsof_output(test_output::EXAMPLE_LSOF_OUTPUT).unwrap();

    assert_eq!(3, lsof_result.len());
    for key in [9097, 16269, 22397].iter() {
        assert!(lsof_result.contains_key(&key.to_string()));
    }

    let lsof_output_lines = test_output::EXAMPLE_LSOF_OUTPUT.trim().split("\n");
    let mut lsof_output_line_set: Vec<&str> = lsof_output_lines.collect();
    consume_lsof_output(lsof_result, &mut lsof_output_line_set);
    assert_eq!(0, lsof_output_line_set.len());
}

const PROCESS_FIELD_ORDER: [&str; 6] = ["p", "g", "R", "c", "u", "L"];
const FILE_FIELD_ORDER: [&str; 11] = ["f", "a", "l", "t", "G", "D", "o", "s", "i", "k", "n"];

macro_rules! assemble_line {
    ($field_order:expr, $entry:expr) => {{
        let mut line = String::new();
        for process_field in $field_order {
            if let Some(content) = $entry.get(&String::from(*process_field)) {
                line += format!("{}{}\0", process_field, content).as_str();
            }
        }
        line
    }};
}

fn consume_lsof_output(lsof_result: parser::LsofOutput, lsof_output_line_set: &mut Vec<&str>) {
    for entry in lsof_result {
        let (_, value) = entry;
        let process_line = assemble_line!(&PROCESS_FIELD_ORDER, &value.process_fields);
        remove_line_from_set(lsof_output_line_set, process_line);
        for file in value.files {
            let file_line = assemble_line!(&FILE_FIELD_ORDER, &file);
            remove_line_from_set(lsof_output_line_set, file_line);
        }
    }
}

fn remove_line_from_set(lsof_output_line_set: &mut Vec<&str>, process_line: String) {
    let position = lsof_output_line_set
        .iter()
        .position(|&l| l == process_line)
        .unwrap();
    drop(lsof_output_line_set.remove(position));
}
