use super::parser_error::ParserError;
use pest::Parser;
use std::collections::HashMap;

#[derive(Parser)]
#[grammar = "lsof.pest"]
struct LsofParser;

#[derive(Default)]
pub struct ProcessEntry {
    pub process_fields: HashMap<String, String>,
    pub files: Vec<HashMap<String, String>>,
}

pub type LsofOutput = HashMap<String, ProcessEntry>;

pub fn parse_lsof_output(input: &str) -> Result<LsofOutput, ParserError> {
    let parser: ParserStateMachine = Default::default();
    parser.parse_lsof_output(input)
}

#[derive(Default)]
struct ParserStateMachine {
    process_map: LsofOutput,
    process_entry: Option<ProcessEntry>,
    pid: String,
    current_files_record: Option<HashMap<String, String>>,
}

#[derive(PartialEq)]
enum Modes {
    CollectingProcessFields,
    CollectingFiles,
}

impl ParserStateMachine {
    pub fn parse_lsof_output(mut self, input: &str) -> Result<LsofOutput, ParserError> {
        let pairs = LsofParser::parse(Rule::file, input)?;
        self.parse_pairs(pairs)?;

        // add record data from processing in progress,
        // as we start with an empty data set and add data when we find the next record
        self.add_file_record();
        self.add_process_map_entry(String::new());

        Ok(self.process_map)
    }

    fn parse_pairs(&mut self, pairs: pest::iterators::Pairs<Rule>) -> Result<(), ParserError> {
        for pair in pairs {
            let records = pair.into_inner();
            self.parse_records(records)?;
        }
        Ok(())
    }

    fn parse_records(&mut self, records: pest::iterators::Pairs<Rule>) -> Result<(), ParserError> {
        for record in records {
            if record.as_rule() != Rule::record {
                continue;
            }
            let fields = record.into_inner();
            self.parse_fields(fields)?;
        }
        Ok(())
    }

    fn parse_fields(&mut self, fields: pest::iterators::Pairs<Rule>) -> Result<(), ParserError> {
        let mut mode: Option<Modes> = None;
        for field in fields {
            let field_parts = field.into_inner();
            let (name, content) = self.parse_field_parts(field_parts)?;

            if mode == None {
                mode = self.initialize_mode(&name, &content);
            }
            self.register_new_content(&mode, name, content)?;
        }
        Ok(())
    }

    fn parse_field_parts(
        &mut self,
        field_parts: pest::iterators::Pairs<Rule>,
    ) -> Result<(String, String), ParserError> {
        let mut name: String = String::new();
        let mut content: String = String::new();
        for field_part in field_parts {
            match field_part.as_rule() {
                Rule::name => name = String::from(field_part.as_str()),
                Rule::content => content = String::from(field_part.as_str()),
                rule => return Err(ParserError::UnexpectedRule(rule)),
            };
        }
        Ok((name, content))
    }

    fn initialize_mode(&mut self, name: &str, content: &str) -> Option<Modes> {
        let mut mode: Option<Modes> = None;
        if name == "p" {
            self.add_process_map_entry(String::from(content));

            mode = Some(Modes::CollectingProcessFields);
        } else if name == "f" {
            self.add_file_record();

            mode = Some(Modes::CollectingFiles);
        }
        mode
    }

    fn register_new_content(
        &mut self,
        mode: &Option<Modes>,
        name: String,
        content: String,
    ) -> Result<(), ParserError> {
        match mode {
            None => {
                return Err(ParserError::UnknownRecordType(name));
            }
            Some(Modes::CollectingProcessFields) => {
                if let Some(current_process_entry_object) = &mut self.process_entry {
                    current_process_entry_object
                        .process_fields
                        .insert(name, content);
                }
            }
            Some(Modes::CollectingFiles) => {
                if let Some(current_files_record_object) = &mut self.current_files_record {
                    current_files_record_object.insert(name, content);
                }
            }
        }
        Ok(())
    }

    fn add_process_map_entry(&mut self, pid: String) {
        if let Some(current_process_entry_object) = &mut self.process_entry {
            let process_struct = std::mem::take(current_process_entry_object);
            let current_pid = std::mem::replace(&mut self.pid, pid);
            self.process_map.insert(current_pid, process_struct);
        } else {
            self.pid = pid;
            self.process_entry = Some(Default::default());
        }
    }

    fn add_file_record(&mut self) {
        if let Some(current_files_record_object) = &mut self.current_files_record {
            if let Some(current_process_entry_object) = &mut self.process_entry {
                current_process_entry_object
                    .files
                    .push(std::mem::take(current_files_record_object));
            }
        } else {
            self.current_files_record = Some(Default::default());
        }
    }
}
