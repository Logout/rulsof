use super::parser;

type PestError = pest::error::Error<parser::Rule>;

#[derive(Debug)]
pub enum ParserError {
    ParserFailed(PestError),
    UnexpectedRule(parser::Rule),
    UnknownRecordType(String),
}

impl From<PestError> for ParserError {
    fn from(err: PestError) -> ParserError {
        ParserError::ParserFailed(err)
    }
}

impl std::fmt::Display for parser::Rule {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self {
            parser::Rule::EOI => write!(f, "Rule:: EOI"),
            parser::Rule::character => write!(f, "Rule::character"),
            parser::Rule::name => write!(f, "Rule::name"),
            parser::Rule::content => write!(f, "Rule::content"),
            parser::Rule::field => write!(f, "Rule::field"),
            parser::Rule::record => write!(f, "Rule::record"),
            parser::Rule::file => write!(f, "Rule::file"),
        }
    }
}

impl std::fmt::Display for ParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self {
            ParserError::ParserFailed(error) => write!(f, "ParserFailed: {}", error),
            ParserError::UnexpectedRule(rule) => write!(f, "UnexpectedRule: {}", rule),
            ParserError::UnknownRecordType(start) => write!(f, "UnknownRecordType: {}", start),
        }
    }
}

impl std::error::Error for ParserError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self {
            ParserError::ParserFailed(error) => Some(error),
            ParserError::UnexpectedRule(..) => None,
            ParserError::UnknownRecordType(..) => None,
        }
    }
}
