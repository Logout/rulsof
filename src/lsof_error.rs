use super::parser_error::ParserError;

#[derive(Debug)]
pub enum LsofError {
    ExecutionFailed(std::io::Error),
    LsofFailed,
    LsofNoOutput,
    LsofMalformedOutput(std::str::Utf8Error),
    ParserFailed(ParserError),
}

impl From<std::io::Error> for LsofError {
    fn from(err: std::io::Error) -> LsofError {
        LsofError::ExecutionFailed(err)
    }
}

impl From<ParserError> for LsofError {
    fn from(err: ParserError) -> LsofError {
        LsofError::ParserFailed(err)
    }
}

impl From<std::str::Utf8Error> for LsofError {
    fn from(err: std::str::Utf8Error) -> LsofError {
        LsofError::LsofMalformedOutput(err)
    }
}

impl std::fmt::Display for LsofError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self {
            LsofError::ExecutionFailed(error) => write!(f, "ExecutionFailed: {}", error),
            LsofError::LsofFailed => write!(f, "LsofFailed"),
            LsofError::LsofNoOutput => write!(f, "LsofNoOutput"),
            LsofError::LsofMalformedOutput(error) => {
                write!(f, "LsofMalformedOutput: {}", error)
            }
            LsofError::ParserFailed(error) => write!(f, "ParserFailed: {}", error),
        }
    }
}

impl std::error::Error for LsofError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self {
            LsofError::ExecutionFailed(error) => Some(error),
            LsofError::LsofFailed => None,
            LsofError::LsofNoOutput => None,
            LsofError::LsofMalformedOutput(error) => Some(error),
            LsofError::ParserFailed(error) => Some(error),
        }
    }
}
