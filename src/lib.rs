extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate tempfile;

pub mod lsof_error;
pub mod parser;
pub mod parser_error;

use lsof_error::LsofError;
use std::ffi::OsStr;
use std::os::unix::ffi::OsStrExt;
use std::process::Command;

const DEFAULT_EXECUTABLE_PATH: &[u8; 4] = b"lsof";

pub struct Lsof<'a> {
    pub executable_path: &'a OsStr,
}

impl Default for Lsof<'_> {
    fn default() -> Self {
        Self::new()
    }
}

impl<'a> Lsof<'a> {
    pub fn new() -> Lsof<'a> {
        Lsof {
            executable_path: OsStr::from_bytes(DEFAULT_EXECUTABLE_PATH),
        }
    }

    pub fn lsof<I>(&self, args: I) -> Result<parser::LsofOutput, LsofError>
    where
        I: IntoIterator<Item = &'a OsStr>,
    {
        let mut lsof_args = vec![OsStr::new("-F0")];
        lsof_args.extend(args);
        let output = Command::new(self.executable_path)
            .args(lsof_args)
            .output()?;
        if !output.status.success() {
            return Err(LsofError::LsofFailed);
        }
        let lsof_output = output.stdout;
        if lsof_output.is_empty() {
            return Err(LsofError::LsofNoOutput);
        }
        let output_as_str = std::str::from_utf8(&lsof_output)?;
        let result = parser::parse_lsof_output(output_as_str)?;
        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fails_if_lsof_cannot_be_found() {
        let lsof = Lsof {
            executable_path: OsStr::from_bytes(b"/current_dir/no_lsof"),
        };
        let args: Vec<&OsStr> = Vec::new();
        if let Err(error) = lsof.lsof(args) {
            if let LsofError::ExecutionFailed(source) = error {
                assert_eq!(std::io::ErrorKind::NotFound, source.kind());
            } else {
                panic!("error should have a source object");
            }
        } else {
            panic!("path should not exist");
        }
    }

    #[test]
    fn test_lsof_path_initialized_to_default() {
        let lsof = Lsof::new();
        assert!(lsof.executable_path.len() > 0);
    }

    #[test]
    fn test_lsof_call() {
        let lsof = Lsof::new();
        let output = lsof
            .lsof(vec![
                OsStr::new("-p"),
                OsStr::new(&std::process::id().to_string()),
            ])
            .unwrap();
        assert!(output.len() > 0);
    }

    #[test]
    fn test_lsof_call_temp_file() {
        let lsof = Lsof::new();
        let temp_file = tempfile::NamedTempFile::new().unwrap();
        let output = lsof.lsof(vec![temp_file.path().as_os_str()]).unwrap();
        assert_eq!(1, output.len());
        for (key, value) in output.iter() {
            assert_eq!(&std::process::id().to_string(), key);
            let path = std::env::current_exe().unwrap();
            let path_str = path.as_path().file_name().unwrap().to_str().unwrap();
            let process_fields = &value.process_fields;
            // exact match is hard, as e.g. Linux limits the field length for "c"
            assert!(path_str.contains(&process_fields["c"]));
        }
    }
}
